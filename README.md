# python-airflow

Clear install

- pip3 install apache-airflow
- mkdir Airflow
- export AIRFLOW_HOME=`pwd`/Airflow
- airflow db init
- airflow webserver
- mkdir dags
- export AIRFLOW_HOME=`pwd`
- airflow scheduler
- airflow users  create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin


http://localhost:8081/
http://localhost:8080/

Isolated install

- mkdir -p ./dags ./logs ./plugins
- echo -e "AIRFLOW_UID=$(id -u)" > .env
- possible to insert into .env -> AIRFLOW_UID=50000
- init DB -> docker-compose up airflow-init
- run Airflow: docker-compose up
- init Admin like prev.: 
  - docker exec -it python-airflow_airflow-worker_1 bash
  - airflow users  create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin

CLI commands
- docker-compose run airflow-worker airflow info
- https://airflow.apache.org/docs/apache-airflow/stable/usage-cli.html

Create variable 
- airflow variables set my_key "1"
- airflow variables export vars.json
- 
```python
from airflow.models import Variable
def result(ti):
    my_var = Variable.get("my_key")
```

```python
ba = BashOperator(
    task_id="ba",
    bash_command="echo my_key = {{ var.value.my_key }}"
)
```

```python
fetching_data = BashOperator(
    task_id='fetching_data',
    bash_command="echo {{ ti.xcom_pull(task_ids='check-status') }}",
)
```